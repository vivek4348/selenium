package com.exostar.baid;

import java.util.ResourceBundle;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class BaidTest {

	public WebDriver driver;
	private String url1 = "https://baiddev.icert.com/idprov/pages/home/dashboard.faces";
	private String username = "superadmin0";
	private String password = "abcd1234*";

	@Before
	public void setup() throws Exception {
		ResourceBundle rb = ResourceBundle.getBundle("config");
		//System.out.println(rb.getString("username"));
		System.setProperty("webdriver.chrome.driver", "C:\\temp\\chromedriver.exe");
		driver = new ChromeDriver();
	}

	@Test
	public void excution() throws Exception {

		try {			
			driver.navigate().to(url1);			
			driver.manage().window().maximize();			
			driver.findElement(By.id("accessLoginId")).sendKeys(username);			
			driver.findElement(By.id("accessuserpassword")).sendKeys(password);			
			driver.findElement(By.id("LoginBtn")).click();		
			driver.findElement(By.xpath(".//*[@id='primaryTab']/ul/li[6]/a/span")).click();
			Thread.sleep(1000);
			driver.findElement(By.xpath(".//a[contains(.,'Upload Companies')]")).click();
			Thread.sleep(1000);
			
			driver.findElement(By.xpath("//*[@id='loadCompaniesForm:userAppAdmins:0:select']")).click();
			Thread.sleep(1000);	
			driver.findElement(By.xpath("//*[@id='loadCompaniesForm:skipFirstTimeLoginEmail']")).click();
			Thread.sleep(1000);
			driver.findElement(By.xpath("//*[@id='loadCompaniesForm:bsFileUploadId1']")).sendKeys("C:\\Users\\alampallyv\\Desktop\\BUC\\BUC_Sample_Files_Local\\LOCAL\\Valid\\Bulk Upload Companies.csv");
			Thread.sleep(1000);
			driver.findElement(By.xpath("//*[@id='loadCompaniesForm:bsFileUploadId2']")).sendKeys("C:\\Users\\alampallyv\\Desktop\\BUC\\BUC_Sample_Files_Local\\LOCAL\\Valid\\Bulk Upload Company Administrators.csv");
			Thread.sleep(1000);
			driver.findElement(By.xpath("//*[@id='loadCompaniesForm:bsFileUploadId3']")).sendKeys("C:\\Users\\alampallyv\\Desktop\\BUC\\BUC_Sample_Files_Local\\LOCAL\\Valid\\Bulk Upload Company Users.csv");
			Thread.sleep(1000);
			
			driver.findElement(By.xpath("//*[@id='loadCompaniesForm:validateButton']")).click();
			Thread.sleep(1000);
			
			
		} catch (Exception e) {
			e.printStackTrace();			
		}
	}

	@After
	public void end() throws Exception {
		Thread.sleep(5000);
		driver.quit();
	}

}

